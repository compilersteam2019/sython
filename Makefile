DIRS=build bin
$(shell mkdir -p $(DIRS))

all: sython

lex.yy.c: src/sython.l
	flex src/sython.l
	mv lex.yy.c build/lex.yy.c
	cp src/sym_hashing.h build/sym_hashing.h
	cp src/sython.h build/sython.h

sython: lex.yy.c
	gcc -o bin/sython build/lex.yy.c -lfl -ly

clean:
	rm bin/sython build/lex.yy.c build/sython.h build/sym_hashing.h