#define IDENTIFIER          101

#define OPERATOR_PLUS       102      // +
#define OPERATOR_MINUS      103      // -
#define OPERATOR_DIV        104      // /
#define OPERATOR_MUL        105      // *
#define OPERATOR_GT         106      // >
#define OPERATOR_LT         107      // <
#define OPERATOR_GTE        108      // >=
#define OPERATOR_LTE        109      // <=
#define OPERATOR_NE         110      // <>
#define OPERATOR_EQ         111      // =
#define OPERATOR_ASSIGN     112      // :=
#define OPERATOR_AND        113      // and
#define OPERATOR_OR         114      // or
#define OPERATOR_NOT        115      // not
#define OPERATOR_ENDCOND    116      // :

#define LIST_SEPARATOR      117      // ,
#define GROUP_OPEN          118      // (
#define GROUP_CLOSURE       119      // )
#define STATEMENT_CLOSURE   120      // ; or \n

#define KEYWORD_IF          121      // if
#define KEYWORD_ELIF        122      // elif
#define KEYWORD_ELSE        123      // else
#define KEYWORD_WHILE       124      // while
#define KEYWORD_EXIT        125      // exit
#define KEYWORD_DEF         126      // def
#define KEYWORD_PRINT       127      // print
#define KEYWORD_INPUT       128      // input
#define KEYWORD_RETURN      129      // return

#define CHAR                130      // [a-zA-Z]
#define CONST               131      // [0-9]
