%{
#include <stdarg.h>
#include <stdio.h>
#include "sym_hashing.h"
#include "sython.h"

#define MAX_ERROR_MSG_COUNT 255

FILE *new_file; 
void count();
void comment();
void idents_increment();
void new_line();
void debugLog(const char* type, const char* value);
void parse_const_numeric(char* yycopy);
void parse_const_string(char* yycopy);
void parse_identifier(char* yycopy);
void yyerror(char *format, ...);
void validateError(char *yycopy);
void close_statement(char closure_type);
%}

L               [A-Za-z]
D               [0-9]          

identifier     	{L}({L}|{D})*     
const_numeric	("-"?{D}){D}*
const_string	\"(\\.|[^"\\])*\"	
other			[\t\v\n\f\r]+

%%

"if"			{ count();char *yycopy=strdup(yytext); debugLog("keyword", "if"); 
                          addsym( yycopy, idents ); return(KEYWORD_IF); }
"elif"			{ count();char *yycopy=strdup(yytext); debugLog("keyword", "elif"); 
                          addsym( yycopy, idents ); return(KEYWORD_ELIF); }
"else"			{ count();char *yycopy=strdup(yytext); debugLog("keyword", "else"); 
                          addsym( yycopy, idents ); return(KEYWORD_ELSE); }
"while"			{ count();char *yycopy=strdup(yytext); debugLog("keyword", "while"); 
                          addsym( yycopy, idents ); return(KEYWORD_WHILE); }
"exit"			{ count();char *yycopy=strdup(yytext); debugLog("keyword", "exit"); 
                          addsym( yycopy, idents ); return(KEYWORD_EXIT); }
"def"			{ count();char *yycopy=strdup(yytext); debugLog("keyword", "def"); 
                          addsym( yycopy, idents ); return(KEYWORD_DEF); }
"print"			{ count();char *yycopy=strdup(yytext); debugLog("keyword", "print"); 
                          addsym( yycopy, idents ); return(KEYWORD_PRINT); }
"input"			{ count();char *yycopy=strdup(yytext); debugLog("keyword", "input"); 
                          addsym( yycopy, idents ); return(KEYWORD_INPUT); }
"return"		{ count();char *yycopy=strdup(yytext); debugLog("keyword", "return"); 
                          addsym( yycopy, idents ); return(KEYWORD_RETURN); }

"("				{ count(); return(GROUP_OPEN); }
")"				{ count(); return(GROUP_CLOSURE); }
"="				{ count(); return(OPERATOR_EQ); }
"<"				{ count(); return(OPERATOR_LT); }
">"				{ count(); return(OPERATOR_GT); }
","				{ count(); return(LIST_SEPARATOR); }
"<="			{ count(); return(OPERATOR_LTE); }
">="			{ count(); return(OPERATOR_GTE); }
"+"				{ count(); return(OPERATOR_PLUS); }
"-"				{ count(); return(OPERATOR_MINUS); }
"*"				{ count(); return(OPERATOR_MUL); }
"/"				{ count(); return(OPERATOR_DIV); }
"and"			{ count(); return(OPERATOR_AND); }
"or"			{ count(); return(OPERATOR_OR); }
"not"			{ count(); return(OPERATOR_NOT); }
"<>"			{ count(); return(OPERATOR_NE); }
":="			{ count(); return(OPERATOR_ASSIGN); }
":"				{ count(); return(OPERATOR_ENDCOND); }
";"				{ count(); close_statement(';'); return(STATEMENT_CLOSURE); }
"#"				{ count(); comment(); }
{other}			{ count(); }
{const_numeric}	{ count(); char *yycopy=strdup(yytext); parse_const_numeric(yycopy); }
{const_string}	{ count(); char *yycopy=strdup(yytext); parse_const_string(yycopy); }
{identifier}	{ count(); char *yycopy=strdup(yytext); parse_identifier(yycopy); }
.				{ count(); char *yycopy=strdup(yytext); validateError(yycopy); }

%%

int yywrap()
{
	return 1;
}

void main(int argc, char *argv[])
{
	int i = 0;
	int ret_val = 1;

	if (argc < 3) 
	{
		printf("\nUsage: sython -f <input file name>.sy [OPTIONS] \n");
	}
	else
	{
		for (i = 0; i < argc; i++)
		{
			if (strcmp(argv[i], "-f") == 0)
			{
				new_file = fopen(argv[i+1], "r");

				if (new_file == NULL)
				{
					printf("\n<%s> not found.\n", argv[1]);
					break;
				}

				i++;
			}

			if (strcmp(argv[i], "-v") == 0)
				verboseMode = 1;
			
			if (strcmp(argv[i], "-p") == 0)
				printHashingTableMode = 1;
		}

		if (new_file != NULL)
		{
			yyrestart(new_file);

			while(ret_val != 0)
			{
				ret_val = yylex();
			}

			fclose(new_file);
		}

		if (printHashingTableMode == 1)
		{
			printf("Listing the symbols table contents [-p]: \n");
			for (i = 0; i < table_size; i++) {
				if (table[i] == NULL)
					continue;
				printf("\t[sym:%s, ident:%d]\n", table[i]->nam, table[i]->idents);
			}
			printf("End of symbols table\n");
		}

		if (error_detected == 0)
			printf("[Sython-lexer:Success] No lectical errors detected\n");
		else
			printf("[Sython-lexer:Failure] Lectical errors detected\n");
	}
}

void count()
{
	int i;
	char c;

	for(i = 0; yytext[i] != '\0'; i++)
	{
		if (yytext[i] == '\r')
			continue;
		if (yytext[i] == '\v')
			continue;
			
		if (yytext[i] == ' ' && !identations_closed)
			idents_increment();
		else if (yytext[i] == '\t')
			idents_increment();
		else if(yytext[i] == '\n')
			new_line();
		else {
			identations_closed = 1;
		}
	}
}

void validateError(char *yycopy)
{
	if (yycopy[0] != '\0' && yycopy[0] != ' ' && yycopy != NULL)
		yyerror("Unknown token `%s`(%d)", yycopy, (int)yycopy[0]);
}

void parse_const_numeric(char* yycopy)
{
	int yyparsed = atoi(yycopy);

	if (yyparsed < -32768 || yyparsed > 32768) {
		yyerror("The value `%d` exceeds the 16bit range", yyparsed);
		return;
	}

	debugLog("numeric constant", yycopy);
	addsym(yycopy, idents);
}

void parse_const_string(char* yycopy)
{
	debugLog("string constant", yycopy);
	addsym(yycopy, idents);
}

void parse_identifier(char* yycopy)
{
	debugLog("identifier", yycopy);
	addsym(yycopy, idents);
}

void idents_increment()
{
	idents++;
}

void new_line()
{
	close_statement('\n');
	identations_closed = 0;
	idents = 0;
	line++;
}

void close_statement(char closure_type)
{
	char *yycopy = strdup(yytext);
	if (statement_closed == 1)
		statement_closed = 0;

	if (closure_type == '\n')
		statement_closed = 1;
}

void comment()
{
	int c;

	while(c != '\n' && c != EOF)
		c = input();

	new_line();	
}

void debugLog(const char* type, const char* value)
{
	if (verboseMode == 0)
		return;
	
	printf("Detected %s `%s` L:%d,I:%d\n", type, value, line, idents);
}

void yyerror(char *format, ...)
{
	char* message = (char *) malloc(MAX_ERROR_MSG_COUNT * sizeof( char ));;
	va_list message_vars;
    va_start(message_vars, format);
    vsprintf(message, format, message_vars);
    va_end(message_vars);
	error_detected = 1;
	printf("%s L:%d,I:%d\n", message, line, idents);
	free(message);
}