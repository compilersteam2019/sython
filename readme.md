# Sython

## Getting Started

### Prerequisites

1. flex
2. bison

### Installation (Ubuntu 18)

Run the following commands

    apt-get update && apt-get upgrade
    apt-get install flex bison

## Build

* Navigate to project's root directory
* Run `make`

## Usage

`./bin/sython filename.sy`